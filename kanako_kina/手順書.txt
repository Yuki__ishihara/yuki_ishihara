テスト環境作成手順書

1.rootにはいる
su -
vagrant(パスワード)

2.mysql作成
//phppraという名前のDBを作る
mysqladmin -u root -p create phppra
mysql
use phppra;
//devPra:担当者リストDBの作成
create table devPra(member_id int primary key auto_increment,member_name varchar(20),password varchar(20) not null,rank int,create_date timestamp,update_date date,del_flg int default 0);
//updateBat:障害当番履歴DBの作成
create table updateBat(update_id int primary key auto_increment,member_id int not null,start_date date,finish_date date,update_date timestamp);

3.データの入力

メンバー管理画面
insert devPra(member_name,password,rank,update_date)values('kina',1111,1,current_date);
insert devPra(member_name,password,rank,update_date)values('watanabe',2222,2,current_date);
insert devPra(member_name,password,rank,update_date)values('ishihara',3333,3,current_date);
insert devPra(member_name,password,rank,update_date)values('enso',4444,4,current_date);

障害当番表履歴
insert updateBat(member_id,start_date,finish_date)values(3,current_date,current_date);
insert updateBat(member_id,start_date,finish_date)values(2,current_date,current_date);
insert updateBat(member_id,start_date,finish_date)values(1,current_date,current_date);