<?php
function getDb(){
	$dsn ='mysql:dbname=phppra; host=localhost';
	$usr ='root';
	$pass = '';

	try{
		//データベースへの接続を確立
		$db = new PDO($dsn,$usr,$pass);
		//データベース接続時に使用する文字コードをutf8に設定
		$db->exec('SET NAMES uff8');
	}catch(PDOException $e){
		die("接続エラー:{$e->getMessage()}");
	}
	return $db;
}



?>