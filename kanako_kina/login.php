<?php
require_once 'DbManager.php';
require_once 'Encode.php';

//テキストボックスの中身が入力されていれば判定を始める
if(isset($_POST['login'])){
	if(empty($_POST['inputId'])||empty($_POST['inputPass'])){
		$errorMessage="必須項目が入力されていません";
	}else{
		try{
			$password = str_replace(array(" ","　"), "", $_POST['inputPass']);
			$id = str_replace(array(" ","　"), "", $_POST['inputId']);
			//パスワードの形式の判定（半角英数字であるかどうか）
			if(preg_match("/^[a-zA-Z0-9]+$/", $password)){

				if(preg_match("/^[0-9]+$/", $id)){
					//入力されたIDが存在するか確認する
					//データベースへの接続を確立
					$db = getDB();
					//select命令の実行(論理削除されていないユーザを表示)
					$stt = $db->prepare('select password from devPra where member_id=:member_id and del_flg=0');
					//select命令文にポストデータの内容をセット
					$stt->bindValue(':member_id',$id);
					//select命令を実行
					$stt->execute();
					$inputPass = $stt->fetch(PDO::FETCH_ASSOC);
					//echo $inputPass['password'];
					if(!$inputPass['password']){
						//IDが存在しない場合
						$errorMessage = "IDが存在しないかパスワードが間違っています";
					}else{
					//入力されたパスワードが入力されたIDのパスワードと一致するか確認する
						//echo "IDは存在してます！";
						//echo $_POST['inputPass'];
						if($inputPass['password']==$_POST['inputPass']){
							//echo "IDとパスワードが一致しました";
							session_start();
							$_SESSION['inputId']=$id;
							//var_dump($_SESSION);
							header("Location:http://192.168.33.10/errRes.php");
						}else{
							//パスワードが一致しない場合
							$errorMessage = "IDが存在しないかパスワードが間違っています";
						}

					}


				}else{
					$errorMessage="IDは半角数字で入力してください";
				}
			}else{
				$errorMessage="パスワードは半角英数字で入力してください";
			}

		}catch(PDOException $e){
			die("エラーメッセージ:{$e->getMessage()}");
		}
		$db =NULL;

	}
}

?>

<html>
<head>
<title>ログイン情報入力ページ</title>	
</head>
<body>
	<font size="5">ログイン情報入力ページ</font>
	<br />
	<br />
	<br />
	<div><font color="red"><?php echo $errorMessage ?></font></div>
	<form action="" method="post">
		メンバーID：<input type="text" name="inputId" value="<?php echo $_SESSION['inputId'] ?>"> <br />
		パスワード：<input type="password" name="inputPass" value="<?php echo  $_SESSION['inputPass'] ?>"> <br />
		<input id="login" name="login" type ="submit" value="ログイン" />
	</form>
</body>
</html>

<?php
	$errorMessage="";
?>


