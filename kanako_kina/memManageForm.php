<?php
require_once 'DbManager.php';
require_once 'Encode.php';
session_start();
if(!isset($_SESSION['inputId'])){
	header("Location:http://192.168.33.10/login.php");
}
//var_dump($_SESSION);
?>

<?php

//処理の分岐
if($_GET['type']=='edit') {
	//編集リンクが押されたのでテキストボックスに対応した名前とパスを表示
	//$member_id と$passwordに値を入れる
	//$editmember_id= $_GET['member_id'];
	$editmember_id=str_replace(array(" ","　"), "", $_GET['member_id']);
	try {
		$db=getDB();
		$stt = $db->query("select member_name,password from devPra where member_id= $editmember_id");
		$textBox = $stt->fetch(PDO::FETCH_ASSOC);
		//var_dump($textBox['member_name']);
		//var_dump($row['password']);
		$db = NULL;
		$errorMessage='メンバーID:' . $editmember_id . 'の編集を行っています';
	}catch(PDOException $e){
		die("エラーメッセージ:{$e->getMessage()}");
	}	
}elseif(isset($_POST['resi'])&&!empty($_POST['mode'])) {
	//編集した値を変更する
	$editmember_id=$_POST['mode'];
	$editName =str_replace(array(" ","　"), "", $_POST['member_name']);
	$editPass =str_replace(array(" ","　"), "", $_POST['password']);;
	if($editName!=NULL&&$editPass!=NULL){
		if(strlen($editName)<21&&strlen($editPass)<21){
			try{
				$db=getDB();
				$stt = $db->prepare("update devPra set member_name=:member_name,password=:password,update_date=current_date where member_id = $editmember_id");
				//update命令文にポストデータの内容をセット
				$stt->bindValue(':member_name',$editName);
				$stt->bindValue(':password',$editPass);
				//isnert命令を実行
				$stt->execute();
				$db =NULL;
				
			}catch(PDOException $e){
				die("エラーメッセージ：{$e->getMessage()}");
			}
			//header("Location:".$_SERVER['PHP_SELF']);
		}else{
			$errorMessage="メンバー名とパスワードはともに20文字以内にしてください";
		}
		$_POST['mode']='';
		header("Location:".$_SERVER['PHP_SELF']);
		
	}
	
 }elseif($_GET['type']=='delete'){
 	//削除
	$member_id = $_GET['member_id'];
	try {
		//del_flgを1に変更する
		$db=getDB();
		$stt = $db->prepare('update devPra set del_flg = 1,rank=NULL where member_id= :member_id');
		$stt->setFetchMode(PDO::FETCH_ASSOC);
		$stt->bindParam(':member_id',$member_id,PDO::PARAM_INT);
		$stt->execute();
	}catch(PDOException $e){
		die("エラーメッセージ:{$e->getMessage()}");
	}
	//del_flgが0のメンバーにランクを振りなおす
	$db=getDB();
		$stt = $db->query("set @i:=0;update devPra set rank=(@i:=@i+1) where del_flg=0;");
		$textBox = $stt->fetch(PDO::FETCH_ASSOC);
		//var_dump($textBox['member_name']);
		//var_dump($row['password']);
		$db = NULL;


	header("Location:".$_SERVER['PHP_SELF']);
	
}elseif(isset($_POST['resi'])){
	//登録
	//メンバー名、パスワードともに入力されているとき
	
	$resiName =str_replace(array(" ","　"), "", $_POST['member_name']);
	$resiPass =str_replace(array(" ","　"), "", $_POST['password']);
	if(!empty($resiName)&&!empty($resiPass)){
		//var_dump($_POST['member_name']);
		//var_dump($_POST['password']);
		if(strlen($resiName)<21&&strlen($resiPass)<21){

			//rankの値をとってくる
			if(preg_match("/^[a-zA-Z0-9]+$/", $resiName)){
				if(preg_match("/^[a-zA-Z0-9]+$/", $resiPass)){
					//同じ名前の人がいないかどうか確認する
					try{
						$db=getDB();
						$stt = $db->prepare('select count(member_name) as count from devPra where member_name=:member_name and del_flg=0');
						//insert命令文にポストデータの内容をセット
						$stt->bindValue(':member_name',$resiName);
						//isnert命令を実行
						$stt->execute();
						$row = $stt->fetch(PDO::FETCH_ASSOC);
						$count=0;
						$count=$row['count'];
						$db =NULL;
					}catch(PDOException $e){
						die("エラーメッセージ：{$e->getMessage()}");
					}
					echo 'count数'.$count;
					if($count==0){
						try{
							//データベースへの接続を確立
							$db = getDB();
							//select命令の実行(rankの最大値を表示)
							$stt = $db->prepare('select max(rank) as rank from devPra') ;
							$stt->execute();
							//結果セットの内容を順に出力
							$row = $stt->fetch(PDO::FETCH_ASSOC);
							//idの変数	
							$rank = $row['rank']+1;
							$db = NULL;
						}catch(PDOException $e){
							die("エラーメッセージ:{$e->getMessage()}");
						}
						echo 'rank数'.$rank;
						
						try{
							
							$db=getDB();
							$stt = $db->prepare('insert into devPra(member_name,password,rank,update_date) values (:member_name,:password,:rank,current_date)');
							//insert命令文にポストデータの内容をセット
							$stt->bindValue(':member_name',$resiName);
							$stt->bindValue(':password',$resiPass);
							$stt ->bindValue(':rank',$rank);
							//isnert命令を実行
							$stt->execute();
							$db =NULL;
							header("Location:".$_SERVER['PHP_SELF']);
						}catch(PDOException $e){
							die("エラーメッセージ：{$e->getMessage()}");
						}
						

					}else{
						$errorMessage="その名前はすでに使われています";
					}


				}else{
					$errorMessage="パスワードは半角英数字で入力してください";
				}
			}else{
				$errorMessage="メンバー名は半角英数字で入力してください";
			}
			
		}else{
			$errorMessage="メンバー名とパスワードはともに20文字以内にしてください";
		}

	}else{
		$errorMessage ="必須項目を入力してください";
	}
}elseif($_GET['type']=='logout'){
	//sessionを空にする
	$_SESSION=array();
	//セッションクッキーが存在する場合には破棄
	if(isset($_COOKIE[session_name()])){
		setcookie(session_name(),'',time()-3600,'/');
	}
	//セッションを破棄
	session_destroy();
	header("Location:login.php");

}

?>
<html>
<head>
<title>メンバー管理</title>	
</head>
<body>
	<font size="5">メンバー管理</font>
	<div><font color="red"><?php echo $errorMessage ?></font></div><br />
	<form action="memManageForm.php" method="post">
		メンバー名：<input type="text" name="member_name" value="<?php echo $textBox['member_name'] ?>"> <br />
		パスワード：<input type="password" name="password" value="<?php echo $textBox['password'] ?>">
		<input name="mode" type="hidden" value="<?php echo $editmember_id ?>" />
		<input id ="resi" name ="resi" type ="submit" value="登録" />
	</form>
	<table border = "1">
	<tr>
		<th>メンバーID</th><th>メンバー名</th><th>担当順</th><th>編集</th><th>削除</th>
	</tr>
<?php

	try{
		//データベースへの接続を確立
		$db = getDB();
		//select命令の実行(論理削除されていないユーザを表示)
		$stt = $db->prepare('select member_id,member_name,rank from devPra where del_flg = 0 order by rank') ;
		$stt->execute();
		//結果セットの内容を順に出力
		while($row = $stt->fetch(PDO::FETCH_ASSOC)){
		//idの変数	
		$member_id = $row['member_id'];
?>
	<tr>
		<td><?php e($row['member_id']); ?></td>
		<td><?php e($row['member_name']); ?></td>
		<td><?php e($row['rank']); ?></td>
		<td><a href="memManageForm.php?type=edit&member_id=<?php echo $member_id ?>"> 編集 </a></td>
		<td><a href="memManageForm.php?type=delete&member_id=<?php echo $member_id ?>"> 削除 </a></td>
	</tr>
	<?php
		}
		$db = NULL;
	}catch(PDOException $e){
		die("エラーメッセージ:{$e->getMessage()}");
	}
?>
</table>
</body>
<br /><br />
<footer><a href="errRes.php"> 障害当番表  </a>     | メンバー管理 　|  <a href="memManageForm.php?type=logout">ログアウト</a></footer>
</html>

<?php
	$errorMessage ="";
?>