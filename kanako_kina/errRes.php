<?php
require_once 'DbManager.php';
require_once 'Encode.php';
session_start();
if(!isset($_SESSION['inputId'])){
	header("Location:http://192.168.33.10/login.php");
}
//var_dump($_SESSION);

?>

<html>
<head>
<title>当番表</title>	
</head>
<body>
	<font size="4">障害当番表</font><br />
		<?php
			try{
				//データベースへの接続を確立
				$db = getDB();
				//select命令の実行(論理削除されていないユーザを表示)
				$stt = $db->prepare('select member_name from devPra where rank=(select MIN(rank) from devPra)');
				$stt->execute();
				//結果セット
				$row = $stt->fetch(PDO::FETCH_ASSOC);
				//名前を保持する変数	
				$responsible = $row['member_name'];
				$db = NULL;
			}catch(PDOException $e){
				die("エラーメッセージ:{$e->getMessage()}");
			}
		?>

	<br /><br />
	<font size="5"><u>今週の障害当番: <?php echo $responsible ?>さん</u></font>
	<form action="errRes.php" method="post"><br />
		<input type="submit" name=update value="担当者更新" />
	</form>
	<br />
	<table border = "1">
	<tr>
		<th>期間　　　　　　　　</th><th>担当者　　　</th>
	</tr>
<?php

//担当者の総数を数える
try {
		$db=getDB();
		$stt = $db->query("select count(member_id) as count from devPra where del_flg=0");
		$count = $stt->fetch(PDO::FETCH_ASSOC);//担当者の総数
		$db = NULL;
		$countAll=$count['count'];//メンバーの総数
		//echo $countAll;
	}catch(PDOException $e){
		die("エラーメッセージ:{$e->getMessage()}");
	}	


//DBから該当する情報をとる
$number=1;
try{
	for($i =0;$i<4;$i++){
		//countAllの値によって変数を変更する

		if($countAll>=4){
			$set=$i;
			//echo "人数は十分です";
		}elseif($countAll==3){
			//echo "人数は3人です";
			if($i<3){
				$set = $i;
			}else{
				$set = 0;
			}
		}elseif($countAll==2){
			//echo "人数は2人です";
			if($i%2==0){
				$set = 0;
			}else{
				$set = 1;
			}
		}else{
			//echo "人数は1人です";
			$set = 0;
		}
		//echo $set;
		//データベースへの接続を確立
		$db = getDB();
		//select命令の実行(論理削除されていないユーザを表示)
		$stt = $db->prepare("select member_name from devPra where del_flg=0 order by rank limit 1 offset $set") ;
		$stt->execute();
		$resRow = $stt->fetch(PDO::FETCH_ASSOC);
		$resRow = $resRow['member_name'];
		//echo $resRow;
		//echo $resRow;
			//期間を求める
		$dayOfTheWeek=date('D');//曜日
		$count = ($number -1) * 7;//加算する数字の計算

		if($dayOfTheWeek=='Mon'){
			$startCount=$count;
			$finishCount=$count +6;
		}elseif($dayOfTheWeek=='Tue'){
			$startCount=$count-1;
			$finishCount=$count +5;
		}elseif($dayOfTheWeek=='Wed'){
			$startCount=$count-2;
			$finishCount=$count +4;
		}elseif($dayOfTheWeek=='Thu'){
			$startCount=$count -3;
			$finishCount=$count +3;
		}elseif($dayOfTheWeek=='Fri'){
			$startCount=$count-4;
			$finishCount=$count +2;
		}elseif($dayOfTheWeek=='sat'){
			$startCount=$count-5;
			$finishCount=$count +1;
		}else{
			$startCount=$count-6;
			$finishCount=$count;
		}
		$startDay= date("Y/m/d",strtotime("$startCount day"));
		$finishDay=date("Y/m/d",strtotime("$finishCount day"));	
		if($number==1){
			//echo "ナンバーは１のときは色をつける";
?>
	<tr style="background-color:#F5DEB3">
	<td><?php echo $startDay ."～".$finishDay; ?></td>
	<td><?php echo $resRow ?></td>
	</tr>
<?php
		}else{
			//echo "ナンバー２のときは色をつけない";
?>
	<tr>
	<td><?php echo $startDay ."～".$finishDay; ?></td>
	<td><?php echo $resRow ?></td>
	</tr>
<?php
		}
 	$number++;//処理の回数


	}
	$db=NULL;
}catch(PDOException $e){
	die("エラーメッセージ:{$e->getMessage()}");
}

?>
</table>
<a href="hisTable.php">障害当番履歴</a>
<br /><br /><br />
</body>
<footer> 障害当番表  | <a href="memManageForm.php">メンバー管理 </a> 　|  <a href="memManageForm.php?type=logout">ログアウト</a></footer>
</html>


<?php 
//更新ボタンが押されたときの処理
if($_POST['update']){

		try{

			//rankを全て-1する
			$db=getDB();
			$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			//トランザクションを開始
			$db->beginTransaction();
			$db->exec('update devPra set rank=rank-1');
			$db->exec('update devPra,(select max(rank) as rank from devPra) devPra_max set devPra.rank = devPra_max.rank+1 where devPra.rank =0;
');
			//全ての処理が成功したらコミットする
			$db->commit();
			$db =NULL;

		}catch(PDOException $e){
			$db->rollBack();
			die("エラーメッセージ：{$e->getMessage()}");
		}
		
	$_POST['update']="";
	header("Location:".$_SERVER['PHP_SELF']);
}
if($_GET['type']=='logout'){
	//sessionを空にする
	$_SESSION=array();
	//セッションクッキーが存在する場合には破棄
	if(isset($_COOKIE[session_name()])){
		setcookie(session_name(),'',time()-3600,'/');
	}
	//セッションを破棄
	session_destroy();
	header("Location:login.php");

}
?>