<?php
require_once 'DbManager.php';
require_once 'Encode.php';
session_start();
if(!isset($_SESSION['inputId'])){
	header("Location:http://192.168.33.10/login.php");
}
?>
<html>
<head>
	<title>障害当番履歴</title>
</head>
<body>
	<font size="4">障害当番履歴</font><br />


	<table border = "1">
	<tr>
		<th>id　　　</th><th>担当者　　　</th><th>担当した期間</th><th>登録日</th>
	</tr>

<?php

	try{
		//データベースへの接続を確立
		$db = getDB();
		//select命令の実行(履歴に表示する項目の値を取得する)
		$stt = $db->prepare("select b.update_id as update_id,a.member_name as member_name,b.start_date as start_date,b.finish_date as finish_date,b.create_date as create_date from devPra as a join updateBat as b on a.member_id=b.member_id;");
		$stt->execute();
		
		while($resRow = $stt->fetch(PDO::FETCH_ASSOC)){
		$update_id = $resRow['update_id'];
		$member_name = $resRow['member_name'];
			//echo "ナンバーは１のときは色をつける"
		$start_date = date('Y/m/d',strtotime($resRow['start_date']));
		$finish_date =date('Y/m/d',strtotime($resRow['finish_date']));
		$create_date = date('Y/m/d',strtotime($resRow['create_date']));
?>
	<tr>
	<td><?php echo $update_id; ?></td>
	<td><?php echo $member_name; ?></td>
	<td><?php echo $start_date ."～".$finish_date; ?></td>
	<td><?php echo $create_date ?></td>
	</tr>
<?php


	}
	$db=NULL;
}catch(PDOException $e){
	die("エラーメッセージ:{$e->getMessage()}");
}

?>
</table>







</body>
<footer> <a href="errRes.php"> 障害当番表  </a> |  <a href="memManageForm.php">メンバー管理 </a> 　|  <a href="memManageForm.php?type=logout">ログアウト</a></footer>
</html>