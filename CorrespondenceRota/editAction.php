<?php
require_once "DutyDbManager.php";
require_once "Encode.php";
require_once "Util.php";
session_start();
checkLogin();
  
  ini_set( 'display_errors', 1 );
?>


<html>
<head>
	<title>処理結果</title>
</head>
<body>
<?php
//var_dump($_POST['member_id']);
//var_dump($_POST['member_name']);
//var_dump($_POST['password']);
$str = strlen($_POST['password']);
// 未入力チェック
if (isEmpty($_POST['member_name'])
 || isEmpty($_POST['password'])) {
	print  '入力された値がありません';

} elseif(checkDistinctEdit($_POST['member_name'],$_POST['member_id'])) {

	print 'メンバー名が重複しています';
}elseif ($str < 4 ) {
	print 'パスワードは4文字以上で入力してください';
} else {
	try{

		$db = getDb();

		$stt = $db->prepare('UPDATE member_list SET member_name = :name , password = :password , update_date = :update_date where member_id = :id');
		$stt->bindValue(':name',$_POST['member_name']);
		$stt->bindValue(':password',$_POST['password']);
		$stt->bindValue(':id',$_POST['member_id']);
		$stt->bindValue(':update_date',date("Y/m/d"));	
		$stt->execute();
		$db = NULL;
		//print "件数:{$stt->rowCount()}";
		print '登録が完了しました';

	
	    

	}	catch(PDOException $e){
		die("接続エラー:{$e->getmessage()}");
	}
}
//header('Location: member_mg.php');
//	exit;
?>
<br />メンバー名：<?php print $_POST['member_name']; ?><br />
パスワード：<?php print $_POST['password']; ?>
<br /><br />
<br /><a href="member_mg.php"> 担当者更新 </a>&nbsp;&nbsp; <a href = "member_mg.php"> メンバー管理 </a> &nbsp;&nbsp;<a href = "logout.php"> ログアウト </a> 
</body>
</html>