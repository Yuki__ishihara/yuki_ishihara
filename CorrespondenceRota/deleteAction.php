<?php
require_once "DutyDbManager.php";
require_once "Encode.php";
require_once "Util.php";
session_start();
ini_set( 'display_errors', 1 );
checkLogin();
?>


<html>
<head>
	<title>処理結果</title>
</head>
<body>
<?php

$id = $_GET['member_id'];
$rank = $_GET['rank'];

	try{

		$db = getDb();

		$stt = $db->prepare('UPDATE member_list SET del_flg = 1 where member_id = :id');
		$stt->bindValue(':id',$_GET['member_id']);
		$stt->execute();

		$db = NULL;

		//print "削除件数:{$stt->rowCount()}";
		print '削除が完了しました<br />';

	}	catch(PDOException $e){
		die("接続エラー:{$e->getmessage()}");
	}
	try{
		
		$db = getDb();
		$stt1 = $db->prepare('UPDATE member_list SET rank = rank -1 WHERE rank > :rank AND del_flg = 0');
		$stt1->bindValue(':rank',$_GET['rank']);
		$stt1->execute();
		$db = NULL;
		//print "更新件数:{$stt1->rowCount()}";
		print '担当順更新が完了しました<br />';

	}	catch(PDOException $e){
		die("接続エラー:{$e->getmessage()}");
	}
	


?>
<br /><a href="member_mg.php"> 担当者更新 </a>&nbsp;&nbsp; <a href = "member_mg.php"> メンバー管理 </a> &nbsp;&nbsp;<a href = "logout.php"> ログアウト </a> 
</body>
</html>