<?php
require_once "DutyDbManager.php";
require_once "Encode.php";
require_once "Util.php";
//ini_set( 'display_errors', 1 );
error_reporting(E_ALL & ~E_NOTICE);
session_start();
checkLogin();

?>
<html>
<head>
	<title>メンバー管理</title>
</head>
<body>

<br /><br />
メンバー管理<br />
<form method="POST" action="member_insert.php">
<p>
メンバー名<br />
 <input type="text" name="member_name" size="25" maxlength="50" />
</p><p>
パスワード（4文字以上）<br />
<input type="password" name="password" size="25" maxlength="10" />
</p><p>
<input type="submit" value="新規登録" /><br /><br />
</p>
</form>

<table border="1">
<tr>
	<th>メンバーID</th><th>メンバー名</th><th>担当順</th><th>編集</th><th>削除</th>
<tr>	

<?php
	
try{
	// DBへ接続
	$db = getDb();
	$stt = $db->prepare('SELECT * FROM member_list WHERE del_flg=0 ORDER BY member_id ');
	$stt->execute();
	while($row = $stt->fetch(PDO::FETCH_ASSOC)){

?>	
	<tr>
		<td><?php e($row['member_id']); ?></td>
		<td><?php e($row['member_name']); ?></td>
		<td><?php e($row['rank']); ?></td>
		<td><a href="member_edit.php?member_id=<?php e($row['member_id']); ?>&member_name=<?php e($row['member_name']); ?>&password=<?php e($row['password']); ?>"> 編集 </a></td>
		<td><a href="deleteAction.php?member_id=<?php e($row['member_id']); ?>&rank=<?php e($row['rank']); ?>"> 削除 </a></td>
		</tr>
	<?php
	}
	$db = NULL;	
}	catch(PDOException $e){
	die("接続エラー:{$e->getmessage()}");
}


?>

</table>
<br /><a href="calendar.php"> 担当者更新 </a>&nbsp;&nbsp; <a href = "member_mg.php"> メンバー管理 </a> &nbsp;&nbsp;<a href = "logout.php"> ログアウト </a> 
</body>
</html>

