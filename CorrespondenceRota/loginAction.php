<?php
	//ini_set( 'display_errors', 1 );
  	//error_reporting(E_ALL & ~E_NOTICE);
  	require_once "DutyDbManager.php";
  	require_once "Encode.php";
  	require_once "Util.php";
	

    function loginAction(){
    	ini_set( 'display_errors', 1 );
  		require_once "DutyDbManager.php";
  		require_once "Encode.php";
  		require_once "Util.php";
    	if (empty($_POST["userid"]) || empty($_POST["password"])) {
    	print $errorMessage = "メンバーIDとパスワードを入力してください";
	  	} else{
	   
	      try{
	      	$db = getDb();
		    $stt = $db->prepare('select password from member_list where member_id = :id');
		    $stt->bindValue(':id',$_POST['userid']);
		    $stt->execute();
		    $row = $stt->fetchAll(PDO::FETCH_NUM);
		    $data = $row[0];
		    //print_r($data[0]);
		    $pass = $data[0];
	      } catch (Exception $e){
	    	die("接続エラー:{$e->getmessage()}");
	      }    

	    if (!$row) {
	      return print('そのメンバーIDは存在しません' . $mysqli->error);
	      $mysqli->close();
	      exit();
	    }

	    // データベースの切断
		$db = NULL;
	    // ３．画面から入力されたパスワードとデータベースから取得したパスワードを比較
	    
	    if ($_POST["password"] === $pass ) {
	    	// ４．認証成功なら、セッションIDを新規に発行する
	      session_regenerate_id(true);
	      $_SESSION["USERID"] = $_POST["userid"];
	      return header("Location: calendar.php");
	      exit;
    } 
	      // 認証失敗
	    return print $errorMessage = "パスワードに誤りがあります";
	}
	  } 
	
?>
